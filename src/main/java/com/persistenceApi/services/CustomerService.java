package com.persistenceApi.services;

import java.util.List;

import com.persistenceApi.models.Customer;

public interface CustomerService {

	public List<Customer> findAll();

	public void save(Customer customer);

	public Customer findOne(Long id);

	public void delete(Long id);
}
